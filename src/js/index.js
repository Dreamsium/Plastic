// open a modal
require(['modal', 'toast'], function (modal, toast) {
  toast('Hello, world!')
  modal('Buy a ticket', 'Flight DS1 flies from Ottawa International to Denver International with a departure time of 10 AM and an arrival time of 4 PM (local times). Would you like to buy a ticket?').then(function (response) {
    console.log(response)
    if (response === true) toast('Flight booked. Check your email.')
  }, function (error) {
    console.error(error)
  })
})
