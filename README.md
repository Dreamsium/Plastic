This is the main development repo for Dreamsium Plastic. 

## Writing a website with Plastic
Plastic isn't a trditional framework like Bootstrap/Material Design Lite/whatever. It uses requirejs and completely modular CSS. To build a website with Plastic, either fork or download this repo, follow the the build instructions, and develop in the src/ folder. 

## Building
Run `npm install` followed by `npm run build` to build from source. 

## Testing
See tests/README.md

## Development Notes
Make sure you add your module to `build.js`.

## License
All code is under the [MIT License](https://opensource.org/licenses/MIT) unless otherwise noted in specific files. It is your responsibility to ensure the rights granted by the licenses are presented on the deployment of the built software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.