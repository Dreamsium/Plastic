/* eslint-disable */
({
  appDir: './src',
  baseUrl: './js',
  dir: './out',
  modules: [
      {
          name: 'index',
          name: 'modal',
          name: 'toast'
      }
  ],
  optimizeCss: 'standard',
  removeCombined: true
})